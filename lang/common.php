<?
$file_rev="062305";
$file_lang="de";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Menu items..
//$LANG_menu_options="Options";
$LANG_menu_options="Optionen";
//$LANG_backtologin="Login";
$LANG_backtologin="Anmelden";
//$LANG_lostpw="Lost Password?";
$LANG_lostpw="Passwort vergessen?";
//$LANG_faq="FAQ";
$LANG_faq="FAQ";
//$LANG_signup="Sign Up";
$LANG_signup="Konto erstellen";
//$LANG_rules="Exchange Rules";
$LANG_rules="Regeln";
//$LANG_tocou="Terms of Service";
$LANG_tocou="Allgemeine Gesch�ftsbedingungen";

//$LANG_menu_extras="Extras";
$LANG_menu_extras="Extras";
//$LANG_topbanns="Top Banners";
$LANG_topbanns="Top Banner";
//$LANG_overallstats="Overall Stats";
$LANG_overallstats="Gesamtstatistiken";

// login page (/index.php)
//$LANG_indtitle="Control Panel Login";
$LANG_indtitle="Konto Login";
//$LANG_headertitle="Banner Exchange Control Panel";
$LANG_headertitle="Banner Exchange Konto";
//$LANG_login_instructions="Enter your login name and password below to access your account. If you would like to join the exchange, <a href=\"cou.php\">Click Here</a> to create a new account!";
//$LANG_login_instructions="Geben Sie unten Ihren Benutzer-Namen und Passwort ein um in Ihr Konto zu gelangen. Falls Sie am Bannerexchange teilnehmen wollen, <a href=\"cou.php\">Klicken Sie hier</a> um ein neues Konto zu erstellen!";
//$LANG_login="Username:";
$LANG_login="Benutzer:";
//$LANG_pw="Password:";
$LANG_pw="Passwort:";
//$LANG_login_button="Login";
$LANG_login_button="Anmelden";

//Recover Password (/recoverpw.php)
//$LANG_lostpw_title="Recover password";
$LANG_lostpw_title="Passwort wiederherstellen";
//$LANG_lostpw_instructions="To reset your password, enter the e-mail address you used to sign up for the exchange. A new password will be generated and e-mailed to this address.";
$LANG_lostpw_instructions="Um Ihr Passwort zur�ckzusetzen, geben Sie bitte die Email-Adresse ein, welche Sie bei der Kontoerstellung angegeben haben. An diese Adresse wird dann ein neu generiertes Passwort gesendet.";
//$LANG_lostpw_instructions2=" For security reasons, it is not possible to send automatically reset passwords to a different e-mail account.";
$LANG_lostpw_instructions2=" Aus Sicherheitsgr�nden ist es nicht m�glich ein automatisch zur�ckgesetztes Passwort an eine andere Email-Adresse zu senden.";
//$LANG_lostpw_recover="Your password has been reset and you will be receiving an e-mail shortly with your new password.";
$LANG_lostpw_recover="Ihr Passwort wurde zur�ckgesetzt. In K�rze erhalten Sie eine Email mit Ihrem neuen Passwort.";
//$LANG_lostpw_email="E-mail Address";
$LANG_lostpw_email="Email Adresse";
//$LANG_lostpw_success="Your password has been reset! You will be receiving an e-mail shortly with your new password. Once you have logged in, you may change your password from your Banner Exchange Control Panel.";
$LANG_lostpw_success="Ihr Passwort wurde zur�ckgesetzt. In K�rze erhalten Sie eine Email mit Ihrem neuen Passwort. Sobald Sie sich erneut angemeldet haben k�nnen Sie Ihr Passwort in Ihrem Kontroll Panel �ndern.";

// Signup Form (/signup.php)
//$LANG_signupwords="Sign up for $exchangename";
$LANG_signupwords="Anmelden f�r $exchangename";
//$LANG_realname="Real Name";
$LANG_realname="Wirklicher Name";
//$LANG_pw_again="Password Again";
$LANG_pw_again="Passwort wiederholen";
//$LANG_cat="Category";
$LANG_cat="Kategorie";
//$LANG_catstuff="Please Select a Category";
$LANG_catstuff="Bitte w�hlen Sie eine Kategorie";
//$LANG_email="Email Address";
$LANG_email="Email Adresse";
//$LANG_siteurl="Site URL";
$LANG_siteurl="URL Ihrer Seite";
//$LANG_bannerurl="Banner URL";
$LANG_bannerurl="URL Ihres Banners";
//$LANG_signsub="Press Once to Submit";
$LANG_signsub="EINMAL Klicken zum Abschicken";
//$LANG_signres="Reset";
$LANG_signres="Zur�cksetzen";
//$LANG_newsletter="Send Newsletter";
$LANG_newsletter="Newsletter senden";
//$LANG_coupon="Coupon Code";
$LANG_coupon="Gutschein Code";
//$LANG_rejected="We could not create your account for the following reasons:";
$LANG_rejected="Aus folgenden Gr�nden konnten wir Ihr Konto nicht erstellen:";

// success messages..
//$LANG_signup_thanks="Thanks for signing up!";
$LANG_signup_thanks="Vielen Dank f�r Ihre Anmeldung!";
//$LANG_signupinfo="Your account has been successfully added! You will be added into the banner rotation in the next couple of days. We have also sent an email to $email with your login and password. You should keep this in a safe place for future reference.  You may also login by going to the <a href=\"index.php\">Stats Login page</a> at any time.  This panel will allow you to add banners, check your stats, change your URL, change your password, etc.<p>";
$LANG_signupinfo="Ihr Konto wurde erfolgreich erstellt! Sie werden dem Bannerexchange innerhalb der n�chsten Tage hinzugef�gt. Weiterhin haben wir Ihnen eine Email an $email mit Ihrem Usernamen und dem Passwort gesendet. Diese sollten Sie f�r den zuk�nftigen Gebrauch an einem sicheren Ort aufbewahren. Au�erdem k�nnen Sie sich jederzeit �ber die <a href=\"index.php\">Login Seite</a> einloggen. Diese �bersicht erlaubt es Ihnen Banner hinzuzuf�gen, Statistiken zu beobachten, Ihre URL zu �ndern, Ihr Passwort zu �ndern usw. ...<p>";
//$LANG_coupon_added="Your account has been credited with <b>$newcredits</b> credits for using the coupon.";
$LANG_coupon_added="Durch das Einl�sen des Gutscheins wurde Ihr Kontostand um <b>$newcredits</b> Kredits erh�ht.";
//$LANG_signup_uploadmsg="Note: You will need to log in and upload a banner in order for your account to be approved!";
$LANG_signup_uploadmsg="Damit Ihr Konto freigegeben werden kann m�ssen Sie sich einloggen und ein Banner hochladen!";

// Conditions of Use page (/conditions.php)
//$LANG_coutitle="Terms and Conditions";
$LANG_coutitle="Allgemeine Gesch�ftsbedingungen";
//$LANG_header="Member Agreement";
$LANG_header="Einverst�ndniserkl�rung";
//$LANG_agree="I Agree";
$LANG_agree="Ich stimme zu";
//$LANG_disagree="I do not agree";
$LANG_disagree="Ich stimme nicht zu";

// Top banners/accounts page (/top.php)
//top 10 banners:
//$LANG_top10_title="Top $topnum accounts";
$LANG_top10_title="Top $topnum Konten";
//$LANG_top10_exposure="Exposures";
$LANG_top10_exposure="Einblendungen";
//$LANG_top10_banners="Banner";
$LANG_top10_banners="Banner";
//$LANG_top10_nobanners="There are no approved banners in the exchange yet, so no data can be shown here! Note: Default banners are NOT shown on this page, only normal user accounts are shown!";
$LANG_top10_nobanners="Im Bannerexchange existieren derzeit keine freigegebenen Banner, deshalb k�nnen hier keine Daten angezeigt werden! ACHTUNG: Default Banner werden auf dieser Seite nicht angezeigt, nur Daten f�r normale Userkonten sind hier zu sehen!";

//Overall stats (/overall.php)
//$LANG_overall_totusers="Users";
$LANG_overall_totusers="Teilnehmer";
//$LANG_overall_exposures="Exposures";
$LANG_overall_exposures="Einblendungen";
//$LANG_overall_banners="Banners";
$LANG_overall_banners="Banner";
//$LANG_overall_totclicks="Clicks to sites";
$LANG_overall_totclicks="Klicks zu dieser Seite";
//$LANG_overall_totsiteclicks="Clicks from sites";
$LANG_overall_totsiteclicks="Klicks von anderen Seiten";
//$LANG_overall_ratio="Click-thru ratio";
$LANG_overall_ratio="Klick Rate";


// common stuff...
//$LANG_yes="Yes";
$LANG_yes="JA";
//$LANG_no="No";
$LANG_no="Nein";
//$LANG_top="Top";
$LANG_top="Top";
//$LANG_topics="Topics";
$LANG_topics="Themen";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>