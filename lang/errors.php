<?
$file_rev="061405";
$file_lang="de";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Error title and headers..
//$LANG_error="ERROR";
$LANG_error="FEHLER";
//$LANG_error_header="The following errors were encountered when processing your request:";
$LANG_error_header="Bei der Abarbeitung Ihres Vorgangs tratt folgender Fehler auf:";
//$LANG_back="Back";
$LANG_back="Zur�ck";
//$LANG_tryagain="Please <a href=\"javascript:history.go(-1)\">go back</a> and try again";
$LANG_tryagain="Bitte <a href=\"javascript:history.go(-1)\">gehen Sie zur�ck</a> und versuchen es erneut";

// Login error stuff
//$LANG_login_error="The login information you entered was incorrect or not found in the database! Please <a href=\"index.php\">go back</a> and try again!";
$LANG_login_error="Ihre Login-Informationen sind entweder falsch oder nicht in unserer Datenbank vorhanden! Bitte <a href=\"index.php\">gehen Sie zur�ck</a> und versuchen es erneut!";
// Client link is different..so we have to repeat this.
//$LANG_login_error_client=//$LANG_login_error="The information you entered was incorrect or not found in the database! Please <a href=\"../index.php\">go back</a> and try again!";
$LANG_login_error_client=//$LANG_login_error="Ihre Login-Informationen sind entweder falsch oder nicht in unserer Datenbank vorhanden! Bitte <a href=\"../index.php\">gehen Sie zur�ck</a> und versuchen es erneut!";

// mysql connect error (used in both login and processing)
//$LANG_error_mysqlconnect="We were unable to connect to the Database with the information provided. The database returned the following error result:";
$LANG_error_mysqlconnect="Mit den angegebenen Informationen konnten wir keine Verbindung zur Datenbank herstellen. Die Datenbank gab folgenden Fehler zur�ck:";

// ADMIN: add account error: We only check the username
// because we expect the admin to know what he/she is doing...
//$LANG_addacct_error="The username exists! Please <a href=\"javascript:history.go(-1)\">go back</a> and try again";
$LANG_addacct_error="Der Username existiert bereits! Bitte <a href=\"javascript:history.go(-1)\">gehen Sie zur�ck</a> und versuchen es erneut";


// ADMIN: add admin errors..
//$LANG_adminconf_login_long="The information you submitted in the Login field is invalid. (should be less than 20 characters in length)";
$LANG_adminconf_login_long="Ihre Angaben im Login Feld sind ung�ltig. (maximale L�nge kleiner 20 Zeichen)";
//$LANG_adminconf_login_short="The information you submitted in the Login field is invalid. (should be at least 2 characters in length)";
$LANG_adminconf_login_short="Ihre Angaben im Login Feld sind ung�ltig. (minimale L�nge 2 Zeichen)";
//$LANG_adminconf_login_inuse="The login ID $newlogin is already in use";
$LANG_adminconf_login_inuse="Die Login ID $newlogin ist bereits vergeben";
//$LANG_adminconf_pw_mismatch="Your passwords do not match";
$LANG_adminconf_pw_mismatch="Ihre Passw�rter stimmen nicht �berein";
//$LANG_adminconf_pw_short="The information you submitted in the password field is invalid. (should be at least 4 characters in length)";
$LANG_adminconf_pw_short="Ihre Angaben im Passwort Feld sind ung�ltig. (minimale L�nge 4 Zeichen)";
//$LANG_adminconf_goback="Please <a href=\"javascript:history.go(-1)\">go back</a> and try again";
$LANG_adminconf_goback="Bitte <a href=\"javascript:history.go(-1)\">gehen Sie zur�ck</a> und versuchen es erneut";
//$LANG_adminconf_added="The account has been added";
$LANG_adminconf_added="Das Konto wurde erfolgreich angelegt";

// ADMIN: Category admin errors...
//$LANG_addcat_tooshort="The category name you submitted should be at least 2 characters long";
$LANG_addcat_tooshort="Die angegebene Kategorie mu� mindestens 2 Zeichen lang sein";
//$LANG_addcat_toolong="The Category name you submitted should be no more than 50 characters long";
$LANG_addcat_toolong="Die angegebene Kategorie darf nicht l�nger als 50 Zeichen sein";
//$LANG_addcat_exists="The Category name you chose already exists";
$LANG_addcat_exists="Die angegebene Kategorie existiert bereits";
//$LANG_cats_nocats="There are no available categories! You should have at least 1 category!";
$LANG_cats_nocats="Es sind noch keine Kategorien vorhanden! Sie sollten zumindest eine Kategorie anlegen!";
//$LANG_delcat_default="You can not delete the default category!";
$LANG_delcat_default="Die Default Kategorie kann nicht gel�scht werden!";

// ADMIN/CLIENT: Change pw errors...
//$LANG_pwconfirm_err_mismatch="Your passwords do not match!";
$LANG_pwconfirm_err_mismatch="Die Passw�rter stimmen nicht �berein!";
//$LANG_pwconfirm_err_short="The information you submitted in the password field is invalid. (should be at least 4 characters in length).";
$LANG_pwconfirm_err_short="Ihre Angaben im Passwort Feld sind ung�ltig. (minimale L�nge 4 Zeichen).";
//$LANG_pwconfirm_err_intro="The following errors were encountered when processing your request";
$LANG_pwconfirm_err_intro="Bei der Bearbeitung Ihrer Anfrage sind folgende Fehler aufgetreten";

// ADMIN/CLIENT: Upload/banner Errors...
//$LANG_upload_blank="You did not enter the path to a valid image. Please go back and try again";
$LANG_upload_blank="Sie haben keinen g�ltigen Pfad zu einer Bilddatei angegeben. Bitte gehen Sie zur�ck und probieren es erneut";
//$LANG_upload_not="Your image was not successfully uploaded. Please go back and try again";
$LANG_upload_not="Ihre Bilddatei konnte nicht erfolgreich hochgeladen werden. Bitte gehen Sie zur�ck und probieren es erneut";
//$LANG_err_badimage="The system was unable to locate your banner image with the URL you provided. This is because it's either not an image, or does not exist at the URL you provided (<b>$bannerurl</b>) if you are uploading this image, there could have been a problem uploading the file.  Please check the URL and try again.  Please note that if you are on a free host such as Geocities or Angelfire, you might need to include your banner somewhere on your own page in order for it to be allowed to be remotely linked. Some services don't allow remote linking at all.";
$LANG_err_badimage="Unter der von Ihnen angegebenen URL konnte kein Banner gefunden werden. Entweder handelt es sich bei der Datei nicht um ein Bild, oder es existiert nicht an der angegebenen URL (<b>$bannerurl</b>). Falls Sie das Bild hochgeladen haben, k�nnte hierbei ein Problem aufgetreten sein. Bitten �berpr�fen Sie die URL und probieren es erneut. Falls Sie sich auf einem freien Host wie Geocities oder Angelfire befinden beachten Sie bitte, da� Ihr Banner irgendwo auf Ihrer Homepage eingebunden sein mu� damit darauf verlinkt werden kann. Einige Provider erlauben das Linken sogar grunds�tzlich nicht.";
//$LANG_err_badwidth="Your banner is invalid because it is too wide. Banners for this exchange should be $bannerwidth pixels wide.";
$LANG_err_badwidth="Ihr Banner ist zu breit. Banner f�r diesen Exchange d�rfen eine Breite von $bannerwidth Pixeln haben.";
//$LANG_err_badheight="Your banner is invalid because it is too high. Banners for this exchange should be $bannerheight pixels high.";
$LANG_err_badheight="Ihr Banner ist zu hoch. Banner f�r diesen Exchange d�rfen eine H�he von $bannerheight Pixeln haben.";
//$LANG_err_filesize="Your banner's file size exceeded the maximum file size allowed in the exchange. Banner should be no larger than $max_filesize bytes.";
$LANG_err_filesize="Ihre Bilddatei ist zu gro�. Das Banner darf maximal $max_filesize Bytes gro� sein.";

// Add admin error..
//$LANG_adminconf_loginexist="The login ID specified is already in use!";
$LANG_adminconf_loginexist="Die angegebene Login ID ist bereits vergeben!";

// edit templates error..
//$LANG_editcsstemplate_errornofile="The file specified does not exist! This usually means you have changed the filename or tried to pass a variable with a incorrect filename. For security, this cannot be done. Please go back and try again.";
$LANG_editcsstemplate_errornofile="Die angegebene Datei existiert nicht! Die Ursache f�r das Problem ist meist, da� Sie den Dateinamen ge�ndert haben oder versucht haben eine Variable mit einem falschen Dateinamen zu �bertragen. Dies ist aus Sicherheitsgr�nden nicht m�glich. Bitte gehen Sie zur�ck und versuchen es erneut.";
//$LANG_editcsstemplate_cannotwrite="phpBannerExchange is unable to write to the css.php file. This can be caused by incorrect permissions on the file (should be 755 or 777) or you do not have access to write to this file. Check the file permissions and access rights on the file in question and try again.";
$LANG_editcsstemplate_cannotwrite="phpBannerExchange kann nicht in die Datei css.php schreiben. Dies liegt h�chstwahrscheinlich an falschen Schreib/Lese Berechtigungen f�r die Datei (mu� 755 oder 777 sein) oder Sie haben keinen Zugriff auf diese Datei. �berpr�fen Sie die Datei-Berechtigungen und Zugriffsrechte f�r die fragliche Datei und versuchen es erneut.";

// Promo Manager errors..
//$LANG_promo_noproduct="You did not enter a product name!";
$LANG_promo_noproduct="Sie haben keinen Produktnamen angegeben!";
//$LANG_promo_badcode="You did not enter a code or the code is already in use!";
$LANG_promo_badcode="Sie haben keinen Code angegeben oder der Code ist bereits in gebrauch!";
//$LANG_promo_noval="You must enter a value in the \"Wert\" field in order to use this promo type.";
$LANG_promo_noval="Sie m�ssen einen Wert im  \"Wert\" Feld angeben um diesen Werbe Typ zu verwenden.";
//$LANG_promo_nocreds="You must enter a value in the \"credits\" field in order to use this promo type.";
$LANG_promo_nocreds="Sie m�ssen einen Wert im \"Kredits\" Feld angeben um diesen Werbe Typ zu verwenden.";

// "COMMON"/PUBLIC SECTION ERRORS

// Lost Password error -- unable to locate account.
//$LANG_lostpw_noacct="We were unable to locate an account for <b>{email}</b>. Please try again.";
$LANG_lostpw_noacct="F�r <b>{email}</b> konnte kein Konto gefunden werden. Bitte versuchen Sie es erneut.";

// Signup Errors (/signupconfirm.php)
//$LANG_err_nametooshort="The information you submitted in the <b>Real Name</b> field is invalid. (should be more than 2 characters--You entered <b>$_REQUEST[name]</b>)";
$LANG_err_nametooshort="Ihre Angaben im Feld <b>Wirklicher Name</b> sind ung�ltig. (Mindestl�nge 2 Zeichen - Ihre Eingabe war <b>$_REQUEST[name]</b>)";
//$LANG_err_nametoolong="The information you submitted in the <b>Real Name</b> field is invalid. (should be less than 100 characters--You entered <b>$_REQUEST[name]</b>)";
$LANG_err_nametoolong="Ihre Angaben im Feld <b>Wirklicher Name</b> sind ung�ltig. (Maximall�nge 100 Zeichen - Ihre Eingabe war <b>$_REQUEST[name]</b>)";
//$LANG_err_loginshort="The information you submitted in the <b>Username</b> field is invalid. (should be less than 20 characters in length--You entered <b>$_REQUEST[login]</b>)";
$LANG_err_loginshort="Ihre Angaben im Feld <b>Username</b> sind ung�ltig. (Mindestl�nge 2 Zeichen - Ihre Eingabe war <b>$_REQUEST[login]</b>)";
//$LANG_err_loginlong="The information you submitted in the <b>Username</b> field is invalid. (should be at least 2 characters in length--You entered <b>$_REQUEST[login]</b>)";
$LANG_err_loginlong="Ihre Angaben im Feld <b>Username</b> sind ung�ltig. (Maximall�nge 20 Zeichen - Ihre Eingabe war <b>$_REQUEST[login]</b>)";
//$LANG_err_logininuse="The Username $_REQUEST[login] is already in use";
$LANG_err_logininuse="Der Username $_REQUEST[login] ist bereits vergeben";
//$LANG_err_emailinuse="The <b>e-mail address</b> you specified, <b>$_REQUEST[email]</b>, is already in use. Only one account per e-mail address is allowed";
$LANG_err_emailinuse="Die angegebene <b>Email Adresse</b> <b>$_REQUEST[email]</b> ist bereits in Gebrauch. Pro Email Adresse ist nur ein Konto erlaubt";
//$LANG_err_invalidurl="Site URL is invalid. Please make sure you include the filename (index.html, for example) or a trailing slash (http://www.somesite.com/)! You entered <b>$_REQUEST[targeturl]</b>";
$LANG_err_invalidurl="Die URL zur Seite ist ung�ltig. Bitte stellen Sie sicher, da� der Dateiname enthalten ist (index.html zum Beispiel) und abschlie�ende Slashes angegeben werden m�ssen (http://www.somesite.com/)! Ihre Eingabe war <b>$_REQUEST[targeturl]</b>";
// ??? DOPPELT ??? $LANG_err_badimage="The system was unable to locate your banner image with the URL you provided. This is because it's either not an image, or does not exist at the URL you provided (<b>$_REQUEST[bannerurl]</b>).  Please check the URL and try again.  Please note that if you are on a free host such as Geocities or Angelfire, you might need to include your banner somewhere on your own page in order for it to be allowed to be remotely linked. Some services don't allow remote linking at all.";
// ??? DOPPELT ??? $LANG_err_badwidth="Your banner is invalid because it is <b>$imagewidth</b> pixels wide. Banners for this exchange should be <b>$bannerwidth</b> pixels wide.";
// ??? DOPPELT ??? $LANG_err_badheight="Your banner is invalid because it is <b>$imageheight</b> pixels high. Banners for this exchange should be <b>$bannerheight</b> pixels high.";
//$LANG_err_email="The system was unable to validate your email address because it contains special characters. Please contact the administrator for assistance. (You entered <b>$_REQUEST[email]</b>).";
$LANG_err_email="Ihre Email-Adresse ist ung�ltig, da sie Sonderzeichen enth�lt. F�r Hilfe kontaktieren Sie bitte den Administrator. (Ihre Eingabe war <b>$_REQUEST[email]</b>).";
//$LANG_err_passmismatch="Your passwords do not match! <b>$_REQUEST[pass]</b> does not equal <b>$_REQUEST[pass2]</b>";
$LANG_err_passmismatch="Ihre Passw�rter stimmen nicht �berein! <b>$_REQUEST[pass]</b> ist nicht gleich <b>$_REQUEST[pass2]</b>";
//$LANG_err_passshort="The information you submitted in the <b>Password</b> field is invalid. (should be at least 4 characters in length-- You entered <b>$_REQUEST[pass]</b>).";
$LANG_err_passshort="Ihre Angaben im <b>Passwort</b> Feld sind ung�ltig. (Mindextl�nge 4 zeichen - Ihre Eingabe war <b>$_REQUEST[pass]</b>).";
//$LANG_err_nocoupon="The coupon code you entered is invalid or it is an invalid coupon type! Coupons are CaSe SeNsItIvE!";
$LANG_err_nocoupon="Der eingegebene Gutschein Code ist ung�ltig oder es handelt sich um einen falschen Gutschein Typ! Gutscheine sind CaSe SeNsItIvE!";

// Client coupon errors...
//$LANG_coupon_wrongtype="This type of coupon can not be applied in the online store!";
$LANG_coupon_wrongtype="Dieser Gutschein Typ kann im Online Shop nicht verwendet werden!";
//$LANG_coupon_nocoup="The coupon you entered is invalid! Coupons are CaSe SeNsItIvE!";
$LANG_coupon_nocoup="Der eingegebene Gutschein ist ung�ltig! Gutscheine sind CaSe SeNsItIvE!";

// Promo code errors..
//$LANG_coupon_clntwrongtype="This type of coupon can only be applied in the online store!";
$LANG_coupon_clntwrongtype="Dieser Gutschein Typ kann nur im Online Shop verwendet werden!";
//$LANG_coupon_noreuse="This coupon can not be reused!";
$LANG_coupon_noreuse="Dieser Gutschein kann nicht noch einmal verwendet werden!";
//$LANG_coupon_userwrongtype="You are ineligible to use this coupon! This coupon can only be used at signup!";
$LANG_coupon_userwrongtype="Sie d�rfen diesen Gutschein nicht verwenden! Dieser Gutschein kann nur bei der Erstanmeldung verwendet werden!";
//$LANG_coupon_noreuseyet="You are not eligible to reuse this coupon at this time! This coupon can be reused on $date_placeholder";
$LANG_coupon_noreuseyet="Diesen Gutschein k�nnen Sie derzeit nicht wieder einl�sen! Dieser Gutschein ist erst wieder an folgendem Tag einl�sbar: $date_placeholder";

// Banner delete error
//$LANG_bannerdel_error="The banner could not be deleted because it has already been removed from the exchange! This could be caused by several things. Go back to the Banner display screen and insure that the banner still exists!";
$LANG_bannerdel_error="Das Banner konnte nicht gel�scht werden, da es bereits aus dem Banner Exchange entfernt wurde! Dieses Problem kann mehrere Ursachen haben. Bitte gehen Sie zur�ck zur Anzeigeseite f�r Banner und stellen Sie sicher, da� das Banner noch vorhanden ist!";

// e-mail change error
//$LANG_infoconfirm_invalid="The system was unable to validate your email address because it contains special characters or is invalid. Please try again or contact the administrator for assistance.";
$LANG_infoconfirm_invalid="Ihre Email Adresse ist ung�ltig da Sie Sonderzeichen enth�lt. Bitte versuchen Sie es erneut oder wenden sich an den Administrator.";

 // Password change errors
//$LANG_err_nopassmatch="Your passwords do not match!";
$LANG_err_nopassmatch="Ihre Passw�rter stimmen nicht �berein!";
//$LANG_err_passtooshort="The information you submitted is invalid. (should be at least 4 characters in length.";
$LANG_err_passtooshort="Ihre Angaben sind ung�ltig. (Minimall�nge 4 Zeichen).";
?>