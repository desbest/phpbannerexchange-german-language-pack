<?
$file_rev="061405";
$file_lang="DE";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// admin menu
// titles
//$LANG_menu_acct="Accounts";
$LANG_menu_acct="Konten";
//$LANG_menu_administration="Administration";
$LANG_menu_administration="Administration";
//$LANG_menu_tools="Tools";
$LANG_menu_tools="Tools";
//$LANG_menu_nav="Navigation";
$LANG_menu_nav="Navigation";

// options..
// accounts
//$LANG_menu_valacct="Validate";
$LANG_menu_valacct="�berpr�fen";
//$LANG_menu_addacct="Add Accounts";
$LANG_menu_addacct="Konten hinzuf�gen";
//$LANG_menu_listacct="List Accounts";
$LANG_menu_listacct="Konten auflisten";
//$LANG_menu_changedefault="Default Banner";
$LANG_menu_changedefault="Default Banner";

// administration
//$LANG_menu_mailer="Mailer Manager";
$LANG_menu_mailer="Mail Manager";
//$LANG_menu_categories="Category Admin";
$LANG_menu_categories="Kategorie Administration";
//$LANG_menu_editpass="Change Password";
$LANG_menu_editpass="Passwort �ndern";
//$LANG_menu_addadmin="Add/remove Admin";
$LANG_menu_addadmin="Administrator hinzuf�gen/l�schen";

// tools
//$LANG_menu_dbtools="Database Tools";
$LANG_menu_dbtools="Datenbank Tools";
//$LANG_menu_templates="Edit Templates";
$LANG_menu_templates="Templates bearbeiten";
//$LANG_editvars_title="Edit Variables";
$LANG_editvars_title="Variablen editieren";
//$LANG_menu_editcss="Edit Style Sheet";
$LANG_menu_editcss="Style Sheet bearbeiten";
//$LANG_menu_faqmgr="FAQ Manager";
$LANG_menu_faqmgr="FAQ Manager";
//$LANG_menu_checkbanners="Check Banners";
$LANG_menu_checkbanners="Banner �berpr�fen";
//$LANG_menu_editcou="Edit COU";
$LANG_menu_editcou="Gesch�ftsbedingungen bearbeiten";
//$LANG_menu_editrules="Edit Rules";
$LANG_menu_editrules="Regeln bearbeiten";
//$LANG_promo_title="Promo Manager";
$LANG_promo_title="Promotion Manager";
//$LANG_menu_pause="Pause Exchange";
$LANG_menu_pause="Exchange pausieren";
//$LANG_menu_unpause="UnPause Exchange";
$LANG_menu_unpause="Exchange aktivieren";
//$LANG_commerce_title="Store Manager";
$LANG_commerce_title="Shop Manager";
//$LANG_updatemgr_title="Update Manager";
$LANG_updatemgr_title="Update Manager";

// navigation
//$LANG_menu_help="Help";
$LANG_menu_help="Hilfe";
//$LANG_menu_home="Home";
$LANG_menu_home="Home";
//$LANG_menu_logout="Logout";
$LANG_menu_logout="Abmelden";

//Stats Page (/admin/stats.php)
//$LANG_stats_nopend="There are no pending accounts.";
$LANG_stats_nopend="Keine zur Pr�fung ausstehenden Konten vorhanden.";
//$LANG_stats_pend_sing="pending account found.";
$LANG_stats_pend_sing="Zu �berpr�fendes Konto gefunden.";
//$LANG_stats_pend_plur="pending accounts found.";
$LANG_stats_pend_plur="Zu �berpr�fende Konten gefunden.";
//$LANG_stats_title="Admin Control Panel";
$LANG_stats_title="Administrator Einstellungen";
//$LANG_stats_statssnapshot="Snapshot: Stats for";
$LANG_stats_statssnapshot="Snapshot: Statistiken f�r";
//$LANG_stats_valusr="Validated Users";
$LANG_stats_valusr="�berpr�fte User";
//$LANG_stats_pendusr="Pending Users";
$LANG_stats_pendusr="Zu �berpr�fende User";
//$LANG_stats_totexp="Total Exposures";
$LANG_stats_totexp="Einblendungen gesamt";
//$LANG_stats_loosecred="Loose Credits";
$LANG_stats_loosecred="Kredits zur Verf�gung";
//$LANG_stats_totalban="Total Banners Served";
$LANG_stats_totalban="Banner gesamt";
//$LANG_stats_totclicks="Total Clicks to Sites";
$LANG_stats_totclicks="Klicks zur Seite gesamt";
//$LANG_stats_totsicl="Total Clicks From Sites";
$LANG_stats_totsicl="Klicks von Seite gesamt";
//$LANG_stats_overrat="Overall Exchange Ratio";
$LANG_stats_overrat="Exchange Rate gesamt";
//$LANG_stats_pendacct="Pending Accounts";
$LANG_stats_pendacct="Zu pr�fende Konten";
//$LANG_stats_addacct="Add an Account";
$LANG_stats_addacct="Ein Konto hinzuf�gen";

// Paused message for Stats page
//$LANG_exchange_paused="<b>THE EXCHANGE IS PAUSED!</b> This means that only the default banners will be shown. Users will still continue to receive credit for exposures. To resume normal functionality, click the <b>Unpause Exchange</b> link.";
$LANG_exchange_paused="<b>DER EXCHANGE MACHT PAUSE!</b> Das bedeutet, da� nur die Default Banner angezeigt werden. Alle Benutzer erhalten weiterhin Kredits f�r Einblendungen. Um den Bannertausch wieder zu aktivieren, klicken Sie auf den <b>Exchange aktivieren</b> Link.";

// Validate account (/admin/validate.php)
//$LANG_val_instructions="The following account(s) are awaiting validation. To validate an account, click on the account name.";
$LANG_val_instructions="Die folgenden Konten warten auf Pr�fung. Um ein Konto zu �berpr�fen, klicken Sie auf den Konto-Namen.";
//$LANG_val_awaiting is for the number of accounts at the bottom
// of the validation page. (eg: "3 account(s) awaiting validation")
//$LANG_val_awaiting="account(s) awaiting validation.";
$LANG_val_awaiting="Konten warten auf Pr�fung.";
//$LANG_val_noaccts="Sorry, there are currently no accounts awaiting validation.";
$LANG_val_noaccts="Derzeit warten keine Konten auf Pr�fung.";

// Add Account (/admin/addacct.php)
// uses some of the edit account form/lang files
//$LANG_addacct_title="Add an Account";
$LANG_addacct_title="Ein Konto hinzuf�gen";
//$LANG_addacct_msg="The Account has been added!";
$LANG_addacct_msg="Das Konto wurde hinzugef�gt!";
//$LANG_addacct_button="Home";
$LANG_addacct_button="Home";

// The following are shared between the Add Account,
// edit account and validate account pages.
//$LANG_edit_realname="Real Name";
$LANG_edit_realname="Wirklicher Name";
//$LANG_edit_login="Login ID";
$LANG_edit_login="Login ID";
//$LANG_edit_pass="Password";
$LANG_edit_pass="Passwort";
//$LANG_edit_email="Email Address";
$LANG_edit_email="Email Adresse";
//$LANG_edit_category="Category";
$LANG_edit_category="Kategorie";
//$LANG_edit_nocats="The Administrator has not defined any categories, so you can not change your category at this time";
$LANG_edit_nocats="Derzeit sind noch keine Kategorien definiert, deshalb k�nnen Sie im Moment Ihre Kategorie nicht �ndern";
//$LANG_edit_exposures="Exposures";
$LANG_edit_exposures="Einblendungen";
//$LANG_edit_credits="Credits";
$LANG_edit_credits="Kredits";
//$LANG_edit_clicks="Clicks to site";
$LANG_edit_clicks="Klick zu dieser Seite";
//$LANG_edit_siteclicks="Clicks from site";
$LANG_edit_siteclicks="Klicks von dieser Seite";
//$LANG_edit_raw="RAW Mode";
$LANG_edit_raw="RAW Mode";
//$LANG_edit_status="Account Status";
$LANG_edit_status="Konto Status";
//$LANG_edit_approved="Approved";
$LANG_edit_approved="Freigegeben";
//$LANG_edit_notapproved="Not Approved";
$LANG_edit_notapproved="Nicht freigegeben";
//$LANG_edit_defaultacct="Default Account";
$LANG_edit_defaultacct="Default Konto";
//$LANG_edit_sendletter="Send Newsletter";
$LANG_edit_sendletter="Newsletter senden";
//$LANG_edit_button_val="Validate Account";
$LANG_edit_button_val="Konto pr�fen";
//$LANG_edit_button_reset="Reset This Page";
$LANG_edit_button_reset="Diese Seite zur�cksetzen";
//$LANG_edit_button_delraw="Delete RAW HTML";
$LANG_edit_button_delraw="RAW HTML l�schen";
//$LANG_edit_button_del="Delete Account";
$LANG_edit_button_del="Konto l�schen";

// Edit Account (/admin/edit.php)
//$LANG_edit_title="Edit account";
$LANG_edit_title="Konto bearbeiten";
//$LANG_edit_heading="Edit/Validate Account";
$LANG_edit_heading="Konto bearbeiten/pr�fen";
// send e-mail link to the right of the e-mail field
//$LANG_email_button_send="Send Email";
$LANG_email_button_send="Email senden";
//$LANG_edit_saleshist="Credit Sales History";
$LANG_edit_saleshist="Historie verkaufter Kredits";
// for viewing raw mode HTML
//$LANG_edit_raw_current="current";
$LANG_edit_raw_current="aktuell";
//$LANG_edit_button_addban="Add Banner";
$LANG_edit_button_addban="Banner hinzuf�gen";
//$LANG_edit_button="Back to Stats";
$LANG_edit_button="Zur�ck zu den Statistiken";
//$LANG_edit_bannerlink="View/Edit Banners";
$LANG_edit_bannerlink="Banner ansehen/bearbeiten";
//$LANG_stats_banner_hdr="Banners";
$LANG_stats_banner_hdr="Banner";
//$LANG_stats_hdr_add="Add a Banner";
$LANG_stats_hdr_add="Ein Banner hinzuf�gen";
// Banner report at the bottom of the edit page
// eg: "3 active banners found for [accountname]"
//$LANG_edit_banners="active banner(s) found for";
$LANG_edit_banners="aktive Banner gefunden f�r";

// Edit/validate Confirm message:
//$LANG_editconf_msg="The Account has been edited.";
$LANG_editconf_msg="Das Konto wurde bearbeitet.";
//$LANG_valconf_msg="The Account has been validated.";
$LANG_valconf_msg="Das Konto wurde gepr�ft.";

// Banners (/admin/banners.php)
//$LANG_targeturl="Target URL";
$LANG_targeturl="Ziel URL";
//$LANG_filename="Filename";
$LANG_filename="Dateiname";
//$LANG_views="Views";
$LANG_views="Einblendungen";
//$LANG_clicks="Clicks";
$LANG_clicks="Klicks";
//$LANG_bannerurl="Banner URL";
$LANG_bannerurl="Banner URL";
//$LANG_menu_target="Change URL(s)";
$LANG_menu_target="URL(s) �ndern";
//$LANG_button_banner_del="Delete Banner";
$LANG_button_banner_del="Banner l�schen";
//$LANG_banner_instructions="To edit a banner's target URL or banner URL, alter the data in the appropriate field, then click the <b>Change URL(s)</b> button. To delete the banner, click the <b>Delete Banner</b> link. To visit the site specified in the Target URL, click the banner belonging to that target URL.";
$LANG_banner_instructions="Um die Ziel- oder Banner URL eines Banners zu �ndern, passen Sie die Daten im entsprechenden Feld an und klicken den <b>URL(s) �ndern</b> Button an. Um ein Banner zu l�schen, klicken Sie auf den <b>Banner l�schen</b> Link. Um die Seite der Ziel URL zu besuchen klicken Sie einfach auf das entsprechende Banner.";

// this variable displays at the bottom of the "Banners" page.
// eg: "4 banner(s) found for this account"
//$LANG_banner_found="banner(s) found for this account";
$LANG_banner_found="Banner f�r dieses Konto gefunden";
//$LANG_stats_nobanner="No banners found for this account!";
$LANG_stats_nobanner="Keine Banner f�r dieses Konto gefunden!";

// Delete Banner (/admin/deletebanner.php)
//$LANG_delban_title="Delete Banner";
$LANG_delban_title="Banner l�schen";
//$LANG_delban_verbage="Are you sure you want to remove this banner? This is a procedure that cannot be undone.<br>";
$LANG_delban_verbage="Sind Sie sicher, da� Sie dieses Banner l�schen wollen? Dieser Schritt kann nicht r�ckg�ngig gemacht werden.<br>";
//$LANG_delban_go="Yes, Delete This Banner";
$LANG_delban_go="Ja, dieses Banner l�schen";
//$LANG_delbanconf_verbage="The banner has been deleted!";
$LANG_delbanconf_verbage="Das Banner wurde gel�scht!";

// Account Listing page (/admin/listall.php)
//$LANG_listall_title="List All Accounts";
$LANG_listall_title="Alle Konten auflisten";
//$LANG_listall_button_back="Back to Stats";
$LANG_listall_button_back="Zur�ck zu den Statistiken";
//$LANG_listall_default="Default Accounts";
$LANG_listall_default="Default Konten";
//$LANG_listall_nodef="No Default Accounts found.";
$LANG_listall_nodef="Keine Default Konten gefunden.";
//$LANG_listall_def_sing="default account found.";
$LANG_listall_def_sing="Default Konto gefunden.";
//$LANG_listall_def_plur="default accounts found.";
$LANG_listall_def_plur="Default Konten gefunden.";
//$LANG_listall_nonorm="No Normal Accounts found.";
$LANG_listall_nonorm="Keine normalen Konten gefunden.";
//$LANG_listall_norm_head="Normal Active Accounts";
$LANG_listall_norm_head="Normale aktive Konten";
//$LANG_listall_norm_sing="normal account found.";
$LANG_listall_norm_sing="normales Konto gefunden.";
//$LANG_listall_norm_plur="normal accounts found.";
$LANG_listall_norm_plur="normale Konten gefunden.";

// Delete Account Page (/admin/deleteaccount.php)
//$LANG_delacct_title="Delete Account";
$LANG_delacct_title="Konto l�schen";
//$LANG_delacct_verbage="Are you sure you want to permenantly delete the account with the login name of";
$LANG_delacct_verbage="Sind Sie sicher da� Sie das folgende Konto dauerhaft l�schen m�chten:";
//$LANG_delacct_go="Yes, Delete Account";
$LANG_delacct_go="Ja, Konto l�schen";
//$LANG_delacct_done="The following account has been deleted:";
$LANG_delacct_done="Das folgende Konto wurde gel�scht:";

// Default Banner (/admin/changedefaultbanner.php)
//$LANG_menu_changedefault="Default Banner";
$LANG_menu_changedefault="Default Banner";
//$LANG_changedefault_title="Change Default Banner";
$LANG_changedefault_title="Default Banner �ndern";
//$LANG_changedefault_message="This function changes the default banner when no banners are eligible for display for some reason. This is not the same as default accounts, as a default account is an unmetered account. The default banner is currently set to:";
$LANG_changedefault_message="Diese Funktion �ndert das Default Banner. Default Banner werden angezeigt, wenn keine anderen Banner f�r die Anzeige zur Verf�gung stehen. Dabei handelt es sich nicht um Default Konten, diese sind unbewertete Konten. Das Default Banner ist derzeit wie folgt gesetzt:";
//$LANG_changedefault_url="Target";
$LANG_changedefault_url="Ziel";
//$LANG_changedefault_nodefault="There is not currently a default banner set!";
$LANG_changedefault_nodefault="Derzeit ist kein Default Banner gesetzt!";
//$LANG_changedefault_bannerurl="Banner URL";
$LANG_changedefault_bannerurl="Banner URL";

// Email All Accounts (/admin/email.php)
//$LANG_email_title="Email All Accounts";
$LANG_email_title="Email an alle Konten";
//$LANG_email_override="Override User Preferences";
$LANG_email_override="Usereinstellungen �berscheiben";
//$LANG_email_override_warning="Use this option *SPARINGLY*";
$LANG_email_override_warning="Diese Option mit *bedacht* verwenden";
//$LANG_email_message="Message:<p>See the Help file for information regarding these variable functions. Valid Variables are";
$LANG_email_message="Message:<p>F�r Informationen zu den Variablen-Funktionen schauen Sie bitte in die Hilfe. G�ltige Variablen sind";
//$LANG_email_button_reset="Reset this Page";
$LANG_email_button_reset="Diese Seite zur�cksetzen";
//$LANG_email_address="E-mail address";
$LANG_email_address="Email Adresse";
//$LANG_email_user="E-mail a User";
$LANG_email_user="Email an User";
//$LANG_email_senttouser="An e-mail has been sent to";
$LANG_email_senttouser="Eine Email wurde gesendet an";
//$LANG_email_return="Click here</a> to return to the account edit screen.";
$LANG_email_return="Klicken Sie hier</a> um zur Seite f�r Kontoeinstellungen zur�ckzukehren.";
//$LANG_email_allcats="All Categories";
$LANG_email_allcats="Alle Kategorien";

// Email Sending status page (/admin/emailgo.php)
//$LANG_emailgo_title="Email All Accounts";
$LANG_emailgo_title="Email an alle Konten";
//$LANG_emailgo_msg_all="Emailing <b>all</b> accounts (including those who opted out of mailing list)...this may take a while.";
$LANG_emailgo_msg_all="Email an <b>alle</b> Konten (incl. derer welche sich nicht im Email-Verteiler befinden) ... dieser Vorgang kann eine Weile dauern.";
//$LANG_emailgo_msg_only="Emailing newsletter enabled accounts...this may take a while.";
$LANG_emailgo_msg_only="Email an alle Konten welche den Newsletter empfangen ... dieser Vorgang kann eine Weile dauern.";

// Email confirmation page (/admin/emailsend.php)
//$LANG_emailconf_title="Email All Accounts - CONFIRM";
$LANG_emailconf_title="Email an alle Konten - BEST�TIGEN";
//$LANG_emailconf_subject="Subject";
$LANG_emailconf_subject="Betreff";
//$LANG_emailconf_msg="Message";
$LANG_emailconf_msg="Nachricht";
//$LANG_emailconf_button_send="Send Email";
$LANG_emailconf_button_send="Email versenden";
//$LANG_emailconf_button_reset="Reset This Page";
$LANG_emailconf_button_reset="Diese Seite zur�cksetzen";

// Category Admin (/admin/catmain.php)
//$LANG_catmain_title="Category Management";
$LANG_catmain_title="Kategorie Management";
//$LANG_catmain_header="Current Categories";
$LANG_catmain_header="Aktuelle Kategorien";
//$LANG_catmain_catname="Category Name";
$LANG_catmain_catname="Kategorie Name";
//$LANG_catmain_sites="Sites";
$LANG_catmain_sites="Webseiten";
//$LANG_catmain_addcat="Add a Category";
$LANG_catmain_addcat="Kategorie hinzuf�gen";
//$LANG_catsfound_singular=" category found.";
$LANG_catsfound_singular=" Kategorie gefunden.";
//$LANG_catsfound_plurl=" Kategorien gefunden.";

// Delete Category (/admin/delcat.php)
//$LANG_delcat_title="Delete Category";
$LANG_delcat_title="Kategorie l�schen";
//$LANG_delcat_acctexist="There are <b>$get_count</b> accounts currently in this category. Deleting this category will reset these accounts to the default category.  Are you <b>*SURE*</b> you wish to do this?";
$LANG_delcat_acctexist="Es existieren <b>$get_count</b> Konten welche diese Kategorie verwenden. Wenn Sie die Kategorie l�schen werden diese Konten auf die Default-Kategorie zur�ckgesetzt. M�chten Sie diesen Vorgang trotzdem durchf�hren?";
//$LANG_delcat_sure="Are you sure you wish to delete this category?";
$LANG_delcat_sure="Sind Sie sicher, da� Sie diese Kategorie l�schen m�chten?";
//$LANG_delcat_button="Yes, Delete Category";
$LANG_delcat_button="Ja, Kategorie l�schen";

// Delete Category Confirmation (/admin/delcatconf.php)
//$LANG_delcatconf_reset="Resetting all accounts in defunct category to Default Category (this may take a moment, there are currently <b>$get_num</b> sites that need to be fixed)";
$LANG_delcatconf_reset="Alle Konten dieser Kategorie werden auf die Default Kategorie zur�ckgesetzt (Dieser Vorgang kann eine Weile dauern, derzeit existieren <b>$get_num</b> Seiten welche angepa�t werden m�ssen)";
//$LANG_delcatconf_status="Changing Category for <b>$name</b> account (id: <b>$id</b>)";
$LANG_delcatconf_status="�ndere Kategorie f�r <b>$name</b> Konto (ID: <b>$id</b>)";
//$LANG_delcatconf_success="The Category has been Deleted";
$LANG_delcatconf_success="Die Kategorie wurde gel�scht";

// Edit Category (/admin/editcat.php)
//$LANG_editcat_title="Edit Category";
$LANG_editcat_title="Kategorie bearbeiten";
//$LANG_editcat_catname="Category Name";
$LANG_editcat_catname="Kategorie Name";
//$LANG_editcat_success="The category has been edited!";
$LANG_editcat_success="Die Kategorie wurde ge�ndert!";

// Add Admin page (/admin/addadmin.php)
//$LANG_addadmin_title="Add/remove an Administrator Account";
$LANG_addadmin_title="Hinzuf�gen/L�schen eines Administrator Kontos";
//$LANG_addadmin_list="Current Administrators";
$LANG_addadmin_list="Aktuelle Administratoren";
//$LANG_addadmin_newlogin="New Login";
$LANG_addadmin_newlogin="Neues Login";
//$LANG_addadmin_pass1="Password";
$LANG_addadmin_pass1="Passwort";
//$LANG_addadmin_pass2="Password Again";
$LANG_addadmin_pass2="Passwort wiederholen";

// Change Password page (/admin/editpass.php)
//$LANG_editpass_title="Change Admin Password";
$LANG_editpass_title="Administrator Passwort �ndern";
//$LANG_editpass_newpass="New Password";
$LANG_editpass_newpass="Neues Passwort";
//$LANG_editpass_newpass1="New Password Again";
$LANG_editpass_newpass1="Neues Passwort wiederholen";
//$LANG_editpass_button="Change Password";
$LANG_editpass_button="Password �ndern";
//$LANG_editpass_reset="Reset This Page";
$LANG_editpass_reset="Diese Seite zur�cksetzen";

//Password confirmation page (/admin/pwconfirm.php)
//$LANG_pwconfirm_title="Change Password";
$LANG_pwconfirm_title="Passwort �ndern";
//$LANG_pwconfirm_success="Your password has been changed! Please <a href=\"index.php\">return to the login page</a> and log back in with your new password.";
$LANG_pwconfirm_success="Ihr Passwort wurde ge�ndert! Bitte <a href=\"index.php\">kehren Sie zur Login-Seite zur�ck</a> und melden sich mit Ihrem neuen Passwort wieder an.";

// Database tools.. (/admin/dbtools.php)
//$LANG_db_title="Database Tools";
$LANG_db_title="Datenbank Tools";
//$LANG_db_buname="Backup Set";
$LANG_db_buname="Backup Satz";
//$LANG_db_budate="Date Created";
$LANG_db_budate="Datum";
//$LANG_db_delete="Delete";
$LANG_db_delete="L�schen";
//$LANG_db_backupfiles="Backup files";
$LANG_db_backupfiles="Backup Dateien";
//$LANG_db_newbuset="Create a new backup set";
$LANG_db_newbuset="Neuen Backup Satz erstellen";
//$LANG_db_instructions="To create a backup set, click the \"Create a new backup set\" link (it may take a few moments on large databases). Click on the file name once the set is created to view it in your web browser. Click File/Save in your web browser to save it to your computer.<p><b>WARNING!</b> Because the Database backup directory is world readable and writable, it is strongly recommended you set up an .htaccess file to password protect the directory! At bare minimum, insure the backup files are deleted after you download/view them.";
$LANG_db_instructions="Um einen Backup Satz zu erstellen, klicken Sie auf den \"Neuen Backup Satz erstellen\" Link (F�r gro�e Datenbanken kann dieser Vorgang eine Weile dauern). Sobald der Backup Satz erstellt ist klicken Sie auf den Dateinamen um sich den Satz in Ihrem Webbrowser anzuschauen. W�hlen Sie Datei/Speichern in Ihrem Browser um das Backup auf Ihrem Rechner zu Speichern.<p><b>Achtung!</b> Da das Verzeichnis f�r die Datenbank-Backups f�r alle lesbar ist, sollten Sie das Verzeichnis unbedingt mit einer .htaccess Datei sch�tzen! Zumindest aber sollten Sie die Backup-Dateien nach dem Betrachten bzw. Download l�schen.";
//$LANG_db_restore="Restore";
$LANG_db_restore="Wiederherstellen";
//$LANG_db_upload="Upload a Backup Set";
$LANG_db_upload="Backup Satz hochladen";
//$LANG_db_upload_button="Upload Set";
$LANG_db_upload_button="Satz hochladen";

// Edit Templates (/admin/templates.php)
//$LANG_menu_templates="Edit Templates";
$LANG_menu_templates="Templates bearbeiten";
//$LANG_templates_message="Select a template you wish to edit from the drop-down menu below. The page will then load in the text box. When you are satisfied with your changes, click the Save button to save your template.";
$LANG_templates_message="W�hlen Sie unten aus der Drop-Down-Liste das Temmplate aus welches Sie bearbeiten m�chten. Das Template wird daraufhin in der Textbox angezeigt. Wenn Sie mit Ihren �nderungen zufrieden sind, klicken Sie auf den Speichern-Button um Ihr Template zu speichern.";
//$LANG_templates_warning="WARNING: This really does change your templates and you can really create problems by altering them if you do not know what you are doing! Change them at your own risk!";
$LANG_templates_warning="ACHTUNG: Dieser Vorgang �ndert Ihr Template. Falls Sie nicht genau wissen was Sie tun kann dies zu erheblichen Problemen f�hren! Sie �ndern die Templates auf eigene Gefahr!";
//$LANG_template_box="Template";
$LANG_template_box="Template";
//$LANG_template_choose="Please choose a template..";
$LANG_template_choose="Bitte w�hlen Sie ein Template...";
//$LANG_preview="Preview";
$LANG_preview="Vorschau";
//$LANG_valid_tags="Valid tags are:";
$LANG_valid_tags="Erlaubte Tags sind:";

// Edit variables page (/admin/editvars.php)
//$LANG_editvars_title="Edit Variables";
$LANG_editvars_title="Variablen �ndern";
//$LANG_varedit_dirs="Define the system variables. These are your global parameters that define things such as your exchange ratio, exchange name, administrator e-mail address, etc. See the <a href=\"../docs/install.php\">installation instructions</a> for details.";
$LANG_varedit_dirs="Definieren Sie die Systemvariablen. Dies sind Ihre globalen Einstellungen welche Dinge definieren wie die Exchange Rate, Name des Bannerexchanges, Email-Adresse des Administrator, usw. ... F�r Details schauen Sie bitte in die <a href=\"../docs/install.php\">Installationsanweisungen</a>.";

//$LANG_dbinstall_head="Database Info";
$LANG_dbinstall_head="Datenbank Information";
//$LANG_varedit_dbhost="Database Host";
$LANG_varedit_dbhost="Datenbank Host";
//$LANG_varedit_dblogin="Database Login";
$LANG_varedit_dblogin="Datenbank Login";
//$LANG_varedit_dbpass="Database Password";
$LANG_varedit_dbpass="Datenbank Passwort";
//$LANG_varedit_dbname="Database Name";
$LANG_varedit_dbname="Datenbank Name";

//$LANG_pathing_head="Pathing & Admin Info";
$LANG_pathing_head="Pfad & Administrations Informationen";
//$LANG_varedit_baseurl="Base Exchange URL";
$LANG_varedit_baseurl="Basis Exchange URL";
//$LANG_varedit_baseurl_note="do not include trailing slash";
$LANG_varedit_baseurl_note="Bitte keinen abschlie�enden Slash";
//$LANG_varedit_basepath="Base Path";
$LANG_varedit_basepath="Basis Pfad";
//$LANG_varedit_exchangename="Exchange Name";
$LANG_varedit_exchangename="Name des Exchange";
//$LANG_varedit_sitename="Site Name";
$LANG_varedit_sitename="Name der Webseite";
//$LANG_varedit_adminname="Admin Name";
$LANG_varedit_adminname="Administrator Name";
//$LANG_varedit_adminemail="Admin Email";
$LANG_varedit_adminemail="Administrator Email";

//$LANG_banners_head= "Banners";
$LANG_banners_head= "Banner";
//$LANG_varedit_width="Banner Width";
$LANG_varedit_width="Banner Breite";
//$LANG_varedit_height="Banner Height";
$LANG_varedit_height="Banner H�he";
//$LANG_varedit_pixels="pixels";
$LANG_varedit_pixels="Pixel";
//$LANG_varedit_defrat="Default Ratio";
$LANG_varedit_defrat="Default Rate";
//$LANG_varedit_showimage="Show Exchange Image";
$LANG_varedit_showimage="Exchange Bild anzeigen";
//$LANG_varedit_imageurl="Exchange Image URL";
$LANG_varedit_imageurl="Exchange Bild URL";
//$LANG_left="Left";
$LANG_left="Links";
//$LANG_right="Right";
$LANG_right="Rechts";
//$LANG_top="Top";
$LANG_top="Oben";
//$LANG_bottom="Bottom";
$LANG_bottom="Unten";
//$LANG_varedit_imageurl_msg="full URL required";
$LANG_varedit_imageurl_msg="volle URL ben�tigt";
//$LANG_varedit_showtext="Show Exchange Link";
$LANG_varedit_showtext="Exchange Link anzeigen";
//$LANG_varedit_exchangetext="Text";
$LANG_varedit_exchangetext="Text";
//$LANG_varedit_reqapproval="Require Banner Approval";
$LANG_varedit_reqapproval="Bannerfreigabe ben�tigt";
//$LANG_varedit_upload="Allow Uploads";
$LANG_varedit_upload="Uploads erlauben";
//$LANG_varedit_maxsize="Maximum Filesize";
$LANG_varedit_maxsize="Maximale Dateigr��e";
//$LANG_varedit_uploadpath="Upload Path (No trailing slashes)";
$LANG_varedit_uploadpath="Upload Pfad (keine abschlie�enden Slashes)";
//$LANG_varedit_upurl="Upload directory URL";
$LANG_varedit_upurl="Upload Verzeichnis URL";
//$LANG_varedit_maxbanners="Maximum Banners";
$LANG_varedit_maxbanners="Maximale Anzahl von Bannern";

//$LANG_anticheat="Anti-Cheat";
$LANG_anticheat="Anti-Cheat";
//$LANG_varedit_anticheat="Anti-Cheat method";
$LANG_varedit_anticheat="Anti-Cheat Methode";
//$LANG_varedit_cookies="Cookies";
$LANG_varedit_cookies="Cookies";
//$LANG_varedit_db="Database";
$LANG_varedit_db="Datenbank";
//$LANG_varedit_none="None";
$LANG_varedit_none="Deine";
//$LANG_varedit_duration="Duration";
$LANG_varedit_duration="Dauer";
//$LANG_varedit_duration_msg="Seconds..";
$LANG_varedit_duration_msg="Sekunden...";

//$LANG_referral_credits="Referral & Credits";
$LANG_referral_credits="Referral & Kredits";
//$LANG_varedit_referral="Referral Program";
$LANG_varedit_referral="Referral Programm";
//$LANG_varedit_bounty="Referral Bounty";
$LANG_varedit_bounty="Referral Bonus";
//$LANG_varedit_startcred="Starting Credits";
$LANG_varedit_startcred="Start Kredits";
//$LANG_varedit_sellcredits="Sell Credits";
$LANG_varedit_sellcredits="Kredits verkaufen";

//$LANG_misc="Miscallaneous";
$LANG_misc="Verschiedenes";
//$LANG_varedit_topnum="Top x will display";
$LANG_varedit_topnum="Top x werden angezeigt";
//$LANG_varedit_topnum_other="Accounts";
$LANG_varedit_topnum_other="Konten";
//$LANG_varedit_sendemail="Send Admin Email";
$LANG_varedit_sendemail="Administrator Email senden";
//$LANG_varedit_usemd5="Use MD5 Encrypted Passwords";
$LANG_varedit_usemd5="MD5 verschl�sselte Passw�rter verwenden";
//$LANG_varedit_usegz="Use gZip/Zend code";
$LANG_varedit_usegz="gZip/Zend Code verwenden";
//$LANG_varedit_userand="Use mySQL4 rand()";
$LANG_varedit_userand="mySQL4 rand() verwenden";
//$LANG_varedit_logclicks="Log Clicks";
$LANG_varedit_logclicks="Klicks loggen";
//$LANG_varedit_userandwarn="Requires mySQL 4 or greater";
$LANG_varedit_userandwarn="Ben�tigt mySQL 4 oder h�her";
//$LANG_date_format="Date Format";
$LANG_date_format="Datums-Format";

// Edit Style Sheet (/admin/editcss.php)
//$LANG_editcss_directionstop="Choose the Style sheet you would like to use with your exchange, then click Submit. Your changes will appear instantly. If you do not see the style sheet you would like to select, upload it to your /templates/css directory under your root exchange directory.";
$LANG_editcss_directionstop="W�hlen Sie das Style Sheet, welches Sie f�r Ihren Bannerexchange verwenden wollen und klicken Sie auf Abschicken. Ihre �nderungen erscheinen sofort. Falls Sie das Style Sheet welches Sie ausw�hlen m�chten nicht vorfinden, laden Sie es in Ihr /templates/css Verzeichnis unterhalb Ihres Root Exchange Verzeichnisses.";
//$LANG_editcss_instructions1="Select the style sheet you would like to edit from the list below, then click Submit. Your sheet will load. Make your changes, then click save to write the file. You must select the edited style sheet (if you have not done so already) after saving for your changes to appear!";
$LANG_editcss_instructions1="W�hlen Sie das Style Sheet, welches Sie bearbeiten wollen und klicken Sie auf Abschicken. Ihr Sheet wird geladen. Machen Sie Ihre �nderungen und klicken Sie auf Speichern um die Datei zu speichern. Falls Sie dies nicht schon getan haben m�ssen Sie das bearbeitet Style Sheet w�hlen, damit nach dem Speichern die �nderungen erscheinen!";
//$LANG_editcss_loadbutton="Load CSS";
$LANG_editcss_loadbutton="CSS laden";

// FAQ Manager (/admin/faq.php)
//$LANG_faq_found="FAQ question(s) found.";
$LANG_faq_found="FAQ Frage(n) gefunden.";
//$LANG_faq_add="Add an article";
$LANG_faq_add="Einen Artikel hinzuf�gen";

// Check Banners (/admin/checkbanners.php)
//$LANG_checkbanners_description="This utility is designed to check all banners and target URLs currently in the Exchange. This tool is useful to clean dead accounts and broken banner links out of your database. If the banner or target URL fails a check, the account is automatically pulled from the rotation and inserted into the validation queue. You may then re-validate these accounts to insure they are valid.<p>To begin checking banners, click the <b>Check Banners</b> link below. Note that it is not necessary to use this utility if you are hosting the banners on your exchange locally.";
$LANG_checkbanners_description="Dieses Utility dient der �berpr�fung aller Banner und Ziel URLs, welche sich derzeit im Bannerexchange befinden. Dieses Tool hilft tote Konten und ung�ltige Bannerlinks aus Ihrer Datenkbank zu entfernen. Falls eine URL den Test nicht besteht, wird das entsprechende Konto automatisch aus der Rotation herausgenommen und mu� neu gepr�ft werden. Sie k�nnen diese Konten dann erneut pr�fen.<p>Um die Banner�berpr�fung zu starten, klicken Sie unten auf den <b>Banner �berpr�fen</b> Link. Beachten Sie, da� Sie diese �berpr�fung nicht durchzuf�hren brauchen, wenn Sie die Banner Ihres Exchanges lokal, d.h. auf Ihrem Server hosten.";
//$LANG_status_OK="OK";
$LANG_status_OK="OK";
//$LANG_status_broken="BROKEN";
$LANG_status_broken="DEFEKT";

// Edit COU/Rules (/admin/editstuff.php)
//$LANG_editstuff_message="HTML is enabled. Consult the documentation for information about what the variables do (they are pretty self-explanatory).<p> Valid Variables are";
$LANG_editstuff_message="HTML ist aktiviert. Wozu die Variablen dienen entnehmen Sie bitte der Dokumentation. Die Variablen sind jedoch recht selbsterkl�rend.<p> M�gliche Variablen sind";
//$LANG_editstuff_result="The page has been edited.";
$LANG_editstuff_result="Die Seite wurde bearbeitet.";
//$LANG_editstuff_url="Click here</a> to see the changes!";
$LANG_editstuff_url="Klicken Sie hier</a> um die �nderungen zu betrachten!";
//$LANG_exchange_paused="<b>THE EXCHANGE IS PAUSED!</b> This means that only the default banners will be shown. Users will still continue to receive credit for exposures. To resume normal functionality, click the <b>Unpause Exchange</b> link.";
$LANG_exchange_paused="<b>EXCHANGE PAUSIERT!</b> Dies bedeutet, da� nur die Defaultbanner angezeigt werden. Die User erhalten aber weiterhin Kredits f�r ihre Einblendungen. Um den Exchange wieder zu aktivieren Klicken Sie bitte auf den <b>Exchange aktivieren</b> Link.";

// Promo/Coupon Manager (/admin/promos.php)
//$LANG_promo_title="Promo Manager";
$LANG_promo_title="Werbe Manager";
//$LANG_promo_noitems="There are currently no active promotional items. You may create new items by using the form below.";
$LANG_promo_noitems="Derzeit sind keine aktiven Werbe Positionen vorhanden. �ber das Formular unten k�nnen Sie neue Positionen erstellen.";
//$LANG_promo_history="View";
$LANG_promo_history="Anzeigen";
//$LANG_promo_name="Promotion Name";
$LANG_promo_name="Name der Werbeaktion";
//$LANG_promo_code="Code";
$LANG_promo_code="Code";
//$LANG_promo_type="Type";
$LANG_promo_type="Typ";
//$LANG_promo_credits="Credits";
$LANG_promo_credits="Kredits";
//$LANG_promo_status="Options";
$LANG_promo_status="Optionen";
//$LANG_promo_timestamp="Start Date";
$LANG_promo_timestamp="Start Datum";
//$LANG_promo_type1="Mass Credits"; 
$LANG_promo_type1="Mengen Kredits"; 
//$LANG_promo_type2="off item"; // percentage off item. expressed as xx% off item.
$LANG_promo_type2="Rabatt"; // percentage off item. expressed as xx% off item.
//$LANG_promo_type3="Special item";
$LANG_promo_type3="Sonderposition";
//$LANG_promo_add="Add a Promotion";
$LANG_promo_add="Werbeaktion hinzuf�gen";
//$LANG_promo_value="Value (if needed)";
$LANG_promo_value="Wert (falls notwendig)";
//$LANG_promo_reuse="Users can re-use coupon code";
$LANG_promo_reuse="User k�nnen Gutschein-Code wiederverwenden";
//$LANG_promo_reuseint="Reuse Interval";
$LANG_promo_reuseint="Intervall wiederbenutzen";
//$LANG_promo_reusedays="Days";
$LANG_promo_reusedays="Tage";
//$LANG_promo_usertype="Eligible User Type";
$LANG_promo_usertype="M�glicher User Typ";
//$LANG_promo_newonly="New Users Only";
$LANG_promo_newonly="Nur neue User";
//$LANG_promo_all="All Users";
$LANG_promo_all="Alle Users";
//$LANG_promo_listall="List All";
$LANG_promo_listall="Alle Auflisten";
//$LANG_promo_listdel="List Deleted";
$LANG_promo_listdel="Gel�schte auflisten";
//$LANG_promo_listact="List Active";
$LANG_promo_listact="Aktive auflisten";
//$LANG_promo_deleted="DELETED"; // Status of promo displayed in name field
$LANG_promo_deleted="GEL�SCHT"; // Status of promo displayed in name field
//$LANG_promo_active="ACTIVE";
$LANG_promo_active="AKTIV";

// Promo Details.. (/admin/promodetails.php)
//$LANG_promodet_title="Coupon - Detailed View";
$LANG_promodet_title="Gutschein - Detailansicht";
//$LANG_promodet_overview="Coupon Overview";
$LANG_promodet_overview="Gutschein �berblick";
//$LANG_promodet_loghead="Usage Log";
$LANG_promodet_loghead="Verwendung";
//$LANG_promodet_nostats="This coupon has not been used. Stats will be available once the coupon has been used by a user.";
$LANG_promodet_nostats="Dieser Gutschein wurde noch nicht verwendet. Die Statistiken k�nnen eingesehen werden sobald der Gutschein von einem User verwendet wurde.";
//$LANG_promodet_id="Coupon ID";
$LANG_promodet_id="Gutschein ID";
//$LANG_promodet_name="Coupon Name";
$LANG_promodet_name="Gutschein Name";
//$LANG_promodet_type="Coupon Type";
$LANG_promodet_type="Gutschein Typ";
//$LANG_promodet_code="Coupon Code";
$LANG_promodet_code="Gutschein Code";
//$LANG_promodet_vals="Coupon Value";
$LANG_promodet_vals="Gutschein Wert";
//$LANG_promodet_credits="Coupon Credits";
$LANG_promodet_credits="Gutschein Kredits";
//$LANG_promodet_reuse="User May Reuse";
//$LANG_promodet_reuse="User darf wiederbenutzen";
//$LANG_promodet_reuseint="Reuse Blackout Expire"; ???
$LANG_promodet_reuseint="Zeit der Wiederverwendbarkeit";
//$LANG_promodet_reuseintdays="days";
$LANG_promodet_reuseintdays="Tage";
//$LANG_promodet_usertype="User Type";
$LANG_promodet_usertype="User Typ";
//$LANG_promodet_timestamp="Created On";
$LANG_promodet_timestamp="Erstellt am";
//$LANG_promodet_status="Coupon Status";
$LANG_promodet_status="Gutschein Status";
//$LANG_promodet_usedate="Date Used";
$LANG_promodet_usedate="Verwendet am";

// Store Manager (/admin/commerce.php)
//$LANG_commerce_title="Store Manager";
$LANG_commerce_title="Shop Manager";
//$LANG_commerce_noitems="There are currently no items available in the store. You may create new items by using the form below.";
$LANG_commerce_noitems="Im Shop sind derzeit keine Positionen vorhanden. �ber das Formular unten k�nnen Sie neue Positionen erstellen.";
//$LANG_commerce_name="Product Name";
$LANG_commerce_name="Produkt Name";
//$LANG_commerce_credits="Credits";
$LANG_commerce_credits="Kredits";
//$LANG_commerce_price="Price";
$LANG_commerce_price="Preis";
//$LANG_commerce_purchased="Purchased";
$LANG_commerce_purchased="Bestellen";
//$LANG_commerce_options="Options";
$LANG_commerce_options="Optionen";
//$LANG_commerce_add="Add an Item";
$LANG_commerce_add="Position hinzuf�gen";
//$LANG_commerce_edititem="Edit Item";
$LANG_commerce_edititem="Position bearbeiten";
//$LANG_commerce_filterhead="Filters";
$LANG_commerce_filterhead="Filter";
//$LANG_commerce_recordsperpage="Records per page";
$LANG_commerce_recordsperpage="Eintr�ge pro Seite";

//$LANG_commerce_view="View/search for transactions";
$LANG_commerce_view="Transaktionen einsehen/suchen";
//$LANG_commerce_notrans="There are no transactions available to view. This could be because there are no transactions in the database or there are no transactions matching the filter you have chosen.";
$LANG_commerce_notrans="Derzeit sind keine Transaktionen vorhanden. Entweder sind keine Transaktionen in der Datenbank oder es existieren keine Transaktionen welche dem gew�hlten Filter entsprechen";
//$LANG_next="Next";
$LANG_next="vor";
//$LANG_previous="Previous";
$LANG_previous="zur�ck";
//$LANG_commerce_invoice="Invoice";
$LANG_commerce_invoice="Rechnung";
//$LANG_commerce_user="User";
$LANG_commerce_user="User";
//$LANG_commerce_item="Item";
$LANG_commerce_item="Position";
//$LANG_commerce_status="Payment Status";
$LANG_commerce_status="Zahlungsstatus";
//$LANG_commerce_payment="Payment Gross";
$LANG_commerce_payment="Brutto Zahlung";
//$LANG_commerce_email="Payer Email";
$LANG_commerce_email="Email des Zahlenden";
//$LANG_commerce_date="Date";
$LANG_commerce_date="Datum";

//$LANG_commerce_filterorders="Show orders with status";
$LANG_commerce_filterorders="Bestellungen mit Status anzeigen";
//$LANG_commerce_uidsearch="Search for orders place by User ID";
$LANG_commerce_uidsearch="Suche nach Bestellungen einer bestimmten User ID";
//$LANG_commerce_go="Go!";
$LANG_commerce_go="Durchf�hren!";
//$LANG_commerce_reset="Clear/reset filters";
$LANG_commerce_reset="Filter l�schen/zur�cksetzen";

// Login Page (/admin/index.php)
//$LANG_index_title="Admin Control Panel Login";
$LANG_index_title="Login Administrator Einstellungen";
//$LANG_index_msg="Banner Exchange<br>Admin Control Panel";
$LANG_index_msg="Banner Exchange<br>Administrator Einstellungen";
//$LANG_index_login="Login";
$LANG_index_login="Login";
//$LANG_index_password="Password";
$LANG_index_password="Passwort";

// Logout Page (/admin/logout.php)
//$LANG_logout_title="Logged Out";
$LANG_logout_title="Abgemeldet";
//$LANG_logout_msg="You have been successfully logged out!<p><a href=\"index.php\">Click Here</a> to return to the login screen.";
$LANG_logout_msg="Sie wurden erfolgreich abgemeldet!<p><a href=\"index.php\">Klicken Sie hier</a> um zur Anmeldeseite zur�ckzukehren.";

// Update page (/admin/update.php)
//$LANG_updatemgr_title="Update Manager";
$LANG_updatemgr_title="Update Manager";
//$LANG_updatemgr_inst="The update manager provides an easy way to check for and obtain updates for phpBannerExchange 2.x. In order for the update manager to work properly, you must make the file \"manifest.php\" world writable (chmod 777). This file is located in your root exchange directory. We will check these permissions for you as part of the update process. Please choose one of the following options:";
$LANG_updatemgr_inst="Der Update Manager bietet eine bequeme M�glichkeit nach Updates f�r phpBannerExchange 2.x zu suchen und diese durchzuf�hren. Damit der Update Manager richtig funktionieren kann, m�ssen Sie die Datei \"manifest.php\" 'world writable' (chmod 777) machen. Sie finden diese Datei im Rootverzeichnis Ihres Exchanges. Im Laufe des Updates werden diese Berechtigungen abgefragt. Bitte w�hlen Sie eine der folgenden Optionen:";
//$LANG_updatemgr_full="Complete refresh/update";
$LANG_updatemgr_full="Komplette Aktualisierung/Update";
//$LANG_updatemgr_fulldesc="Refreshes your manifest file with the currently file version numbers and checks for updates on the master server. (Recommended)";
$LANG_updatemgr_fulldesc="Aktualisiert Ihre Manifest-Datei mit den aktuellen Versionsnummern und sucht nach Updates auf dem Master Server. (empfohlen)";
//$LANG_updatemgr_refresh="Refresh Manifest";
$LANG_updatemgr_refresh="Manifest aktualisieren";
//$LANG_updatemgr_refreshdesc="Refreshes your manifest file with the current file version numbers only.";
$LANG_updatemgr_refreshdesc="Aktualisiert nur Ihre Manifest-Datei mit den aktuellen Versionsnummern.";
//$LANG_updatemgr_updonly="Update Only";
$LANG_updatemgr_updonly="Nur Updaten";
//$LANG_updatemgr_updonlydesc="Obtains updates WITHOUT updating your manifest list. If your manifest is out of date, you will NOT recieve the latest updates!";
$LANG_updatemgr_updonlydesc="Holt Updates OHNE Ihre Manifest-Datei zu aktualisieren. Falls Ihre Manifest-Datei nicht mehr auf dem aktuellsten Stand ist, erhalten Sie NICHT die neuesten Updates!";
//$LANG_updatemgr_checkperms="Checking permissions on the <b>manifest.php</b> file...";
$LANG_updatemgr_checkperms="�berpr�fe Berechtigung f�r <b>manifest.php</b> Datei...";
//$LANG_updatemgr_permok="The file <b>manifest.php</b> is writable. Continuing...";
$LANG_updatemgr_permok="Die Datei <b>manifest.php</b> ist beschreibbar. Verarbeitung geht weiter...";
//$LANG_updatemgr_manifestcheck="Checking file versioning...";
$LANG_updatemgr_manifestcheck="�berpr�fe Datei Versionierung...";
//$LANG_updatemgr_manifeststamp="The manifest was last updated";
$LANG_updatemgr_manifeststamp="Die Manifest-Datei wurde zuletzt aktualisiert";
//$LANG_updatemgr_never="Never";
$LANG_updatemgr_never="Nie";
//$LANG_updatemgr_url="Update URL";
$LANG_updatemgr_url="URL aktualisieren";
//$LANG_updatemgr_popmanifest="Populating Manifest. Please wait...";
$LANG_updatemgr_popmanifest="Versorge Manifest-Datei mit neuen Versions-Nummern. Bitte warten...";

// update manager errors (can't put these in the errors.php file)..
//$LANG_updatemgr_permerror="<b>ERROR!</b> The file <b>manifest.php</b> is NOT writeable by the script! This means you can not update your manifest! Check the permissions on this file and try again (hint: chmod 777 manifest.php).";
$LANG_updatemgr_permerror="<b>FEHLER!</b> Die Datei <b>manifest.php</b> kann vom Script nicht beschrieben werden! Das bedeutet Ihre Manifest-Datei kann nicht aktualisiert werden! �berpr�fen Sie die Berechtigungen f�r diese Datei (Hinweis: chmod 777 manifest.php).";
//$LANG_updatemgr_nomanifest="<b>ERROR!</b> The file <b>manifest.php</b> can't be found by the script! Insure the manifest.php file is located in the root phpBannerExchange folder and try again!";
$LANG_updatemgr_nomanifest="<b>FEHLER!</b> Die Datei <b>manifest.php</b> kann vom Script nicht gefunden werden! Stellen Sie sicher, da� die Datei manifest.php im Rootverzeichnis des Banner Exchanges vorhanden ist und versuchen es erneut!";
//$LANG_updatemgr_successwrite="The <b>manifest.php</b> file has been successfully written! Continuing...";
$LANG_updatemgr_successwrite="Die Datei <b>manifest.php</b> wurde erfolgreich beschrieben! Verarbeitung geht weiter...";
//$LANG_updatemgr_getmaster="Attempting to open a connection to the master file. This make take a moment.";
$LANG_updatemgr_getmaster="Versuche eine Verbindung zur Master Datei herzustellen. Dieser Vorgang kann eine Weile dauern.";

//$LANG_updatemgr_cantconnect="The script was not able to connect to the remote site. This could be because your version of PHP does not support this feature or your server administrator has disabled the \"<b>allow_url_fopen</b>\" directive. It could also be caused by the server being inaccessible at the moment. If the problem persists, please contact your server administrator to insure you are using PHP version 4.3.0 or higher and the <b>allow_url_fopen</b> directive is enabled in your PHP configuration.";
$LANG_updatemgr_cantconnect="Das Script konnte keine Verbindung zum Remoteserver herstellen. Das k�nnte daran liegen, da� Ihre PHP-Version dieses Feature nicht unterst�tzt oder da� Ihr Server-Administrator die \"<b>allow_url_fopen</b>\" Funktion deaktiviert hat. Weiterhin kann der Fehler dadurch verursacht sein, da� der Server nicht erreichbar ist. Falls das Problem weiterhin besteht, kontaktieren Sie Ihren Server-Administrator um sicherzustellen, da� Sie mindesten PHP in der Version 4.3.0 verwenden und die Verwendung der <b>allow_url_fopen</b> Funktion in Ihrer PHP-Konfiguration freigeschaltet ist.";
//$LANG_updatemgr_compare="Processing version information..please wait..";
$LANG_updatemgr_compare="Verarbeite Versionsinformationen ... bitte warten ...";
//$LANG_updatemgr_uptodate="Same version";
$LANG_updatemgr_uptodate="Gleiche Version";
//$LANG_updatemgr_needsupdt="Different version";
$LANG_updatemgr_needsupdt="Andere Version";
// $number files found on the master list.
//$LANG_updatemgr_valsfound="files found on the master list.";
$LANG_updatemgr_valsfound="Dateien in der Master Liste gefunden.";
//$LANG_updatemgr_notupgrade="All of your files are up to date! There are no updated files waiting to be downloaded.";
$LANG_updatemgr_notupgrade="All Ihre Dateien sind auf dem neuesten Stand! Es m�ssen keine Updates durchgef�hrt werden.";
//$LANG_updatemgr_updwaiting="file(s) await upgrading. The following files have been updated and are ready for download:";
$LANG_updatemgr_updwaiting="Dateien warten auf ein Update. Die folgenden Dateien wurden aktualisiert und warten auf den Download:";
//$LANG_updatemgr_updateinst="To install the updates, simply save the files to your computer, rename the .txt extensions to .php, and upload them to the appropriate directory on your server. The folder names are provided next to the file above (eg: \"/index.php\" means this file is your \"index.php\" file located in the root phpBannerExchange directory, \"/admin/validate.php\" is your \"validate.php\" file located in your \"/admin\" folder, etc.";
$LANG_updatemgr_updateinst="Um die Updates zu installieren, speichern Sie einfach die Datein auf Ihrem Rechner, benennen die Dateierweiterung von .txt in .php um, und laden diese in das ensprechende Verzeichnis auf Ihrem Server. Die Verzeichnisnamen werden oben neben den Dateien angezeigt (Beispiel: \"/index.php\" es handelt sich um Ihre Datei \"index.php\" welches sich im phpBannerExchange Rootverzeichnis befindet, \"/admin/validate.php\" ist Ihre \"validate.php\" Datei und befindet sich in Ihrem \"/admin\" Verzeichnis, usw.";
//$LANG_updatemgr_changelog="View Release Notes";
$LANG_updatemgr_changelog="Bemerkungen zum Release ansehen";

// Warnings..
//$LANG_installdir_warning="WARNING! The software has detected that the install folder exists! This is a security risk. It is absolutely imperative that this folder be deleted!";
$LANG_installdir_warning="WARNUNGG! Das Programm hat festgestellt, da� das install Verzeichnis existiert! Das ist ein Sicherheitsrisiko. Es ist unbedingt notwendig da� dieses Verzeichnis gel�scht wird!";

// Generic words used all over the place
//$LANG_yes="Yes";
$LANG_yes="Ja";
//$LANG_no="No";
$LANG_no="Nein";
//$LANG_back="Back";
$LANG_back="Zur�ck";
//$LANG_submit="Submit";
$LANG_submit="Abschicken";
//$LANG_reset="Reset";
$LANG_reset="Zur�cksetzen";
//$LANG_emailing="Emailing";
$LANG_emailing="Sende Emails";
//$LANG_done="Done";
$LANG_done="Erledigt";
//$LANG_ID="ID";
$LANG_ID="ID";
//$LANG_question="Question";
$LANG_question="Frage";
//$LANG_action="Action";
$LANG_action="Aktion";
//$LANG_delete="Delete";
$LANG_delete="L�schen";
//$LANG_edit="Edit";
$LANG_edit="Bearbeiten";
//$LANG_answer="Answer";
$LANG_answer="Antwort";
//$LANG_reactivate="Reactivate";
$LANG_reactivate="Reaktivieren";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!

?>