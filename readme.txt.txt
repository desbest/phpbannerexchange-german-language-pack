Fehler in der ‹bersetzung bitte an info@palmfreeware.de melden!
Please report translation errors to info@palmfreeware.de!

Antek - Palmfreeware.de


History:

Version 1.2, 23.06.05
- Fix: forgotten " in client.php
- Fix: all variables changed from $Lang_... to $LANG_...

Version 1.1, 14.06.05
- Several fixes in all files.

Version 1.0, 13.04.05
- Basic Translation.


